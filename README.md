# Upmeup Rust Backend

I wanted to learn some rust and since I have a pretty complete knowledge of the internal workings of the [Upmeup Backend](https://gitlab.com/librecoop/up-me-up/upmeup-server), I thought it could be a nice project to try to rewrite completely in rust.
