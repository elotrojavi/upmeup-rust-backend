use mongodb::{options::ClientOptions, Client, Database};
use std::sync::OnceLock;

static MONGO_CLIENT: OnceLock<Database> = OnceLock::new();

pub fn get_client() -> Database {
    MONGO_CLIENT
        .get_or_init(|| {
            // TODO: Better error handling, right now it works if there is no connection to the database
            // Read environment variables
            let url = std::env::var("MONGO_URL").unwrap_or("localhost".to_string());
            let port = std::env::var("MONGO_PORT").unwrap_or("27017".to_string());
            let db_name = std::env::var("MONGO_DB").unwrap_or("upmeup_prod".to_string());
            println!("Initializing db...");
            // Parse the connection string into ClientOptions
            let client_options =
                ClientOptions::parse(format!("mongodb://{}:{}/{}", url, port, db_name)).unwrap();

            // Create and return the MongoDB client with options
            let client = Client::with_options(client_options).unwrap();

            client.default_database().unwrap()
        })
        .clone()
}
