use std::str::FromStr;

use crate::types::user::User;
use crate::utils::mongo;
use async_graphql::*;
use bson::doc;
use bson::oid::ObjectId;
const COLLECTION_NAME: &str = "users";

#[derive(Default)]
pub struct UsersQuery;

#[Object]
impl UsersQuery {
    async fn user(&self, id: String) -> Result<Option<User>> {
        let loaded_user = mongo::get_client()
            .collection(COLLECTION_NAME)
            .find_one(doc! { "_id": ObjectId::from_str(&id)? }, None)
            .await?
            .expect("User not found");
        return Ok(bson::from_bson(bson::Bson::Document(loaded_user))?);
    }
}
