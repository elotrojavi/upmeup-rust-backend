use async_graphql::*;

#[derive(Default)]
pub struct OffersQuery;

#[Object]
impl OffersQuery {
    async fn offer(&self) -> Result<String> {
        return Ok(String::from("This is an offer"));
    }
}
