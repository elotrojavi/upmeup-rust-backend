use async_graphql::MergedObject;

mod users;
mod offers;

#[derive(MergedObject, Default)]
pub struct Query(pub users::UsersQuery, pub offers::OffersQuery);
