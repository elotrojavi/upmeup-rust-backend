use async_graphql::*;
use bson::oid::ObjectId;
use serde::{Deserialize, Serialize};

#[derive(SimpleObject, Serialize, Deserialize, Debug)]
pub struct User {
    #[graphql(name = "_id")]
    pub _id: ObjectId,
    pub name: String,
    pub surname: String,
    pub legal_form: Option<String>,
    pub city: String,
    // pub sector: Vec<Sector>,
    #[serde(rename = "eduLevel")]
    pub edu_level: String,
    #[graphql(skip)]
    pub password: String,
    pub r#type: String,
    pub email: String,
    pub website: Option<String>,
    #[serde(rename = "jobPosition")]
    pub job_position: String,
    #[serde(rename = "lastJobTasks")]
    pub last_job_tasks: String,
    pub experience: String,
    pub languages: Vec<String>,
    // pub competencies: Vec<Competencies>,
    // pub soft_skills: Vec<SoftSkill>??,
    // pub preferences: Preferences,
    #[serde(rename = "avatar_B64")]
    #[graphql(name = "avatar_B64")]
    pub avatar_b64: Option<String>,
    pub video: Option<String>,
    pub cv: Option<String>,
    #[graphql(name = "coverLetter")]
    pub cover_letter: Option<String>,
    #[graphql(skip)]
    pub token: Option<String>,
    //#[graphql(skip)]
    // pub token_expiry: Date,
}
